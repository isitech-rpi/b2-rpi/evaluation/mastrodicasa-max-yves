#pragma once
#include "Personne.h"
#include <string>
class EquipeMoto 
{
private :
	string nom;
	Personne* manager;
	Personne* lesPilotes[3];

public :
	unsigned int maxPilote = 3;
	
	EquipeMoto(string nomEquipe, string nomManager);
	~EquipeMoto();

	string GetNom() { return this->nom; }
	string GetNom() const { return this->nom; }
	Personne* GetManager() { return this->manager; }
	Personne* GetManager() const { return this->manager; }

	void SetNom(string nom);
	void AddPilotes(unsigned int rang, Personne* pilot);

	Personne** GetPilotes() { return lesPilotes; };


};

