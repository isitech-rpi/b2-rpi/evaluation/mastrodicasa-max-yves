#include "EquipeMoto.h"

EquipeMoto::EquipeMoto(string nomEquipe, string nomManager) {
	this->nom = nomEquipe;
	this->manager = new Personne(nomManager);
	for (int i = 0;i<3;i++) {
		this->lesPilotes[i] = nullptr;
	}
}

EquipeMoto::~EquipeMoto() { delete this->manager; }  //oups...

void EquipeMoto::SetNom(string nom) {
	this->nom = nom ;
}

void EquipeMoto::AddPilotes(unsigned int rang, Personne* pilot){
	if (rang < 3) {
		this->lesPilotes[rang] = pilot;
	}
}
