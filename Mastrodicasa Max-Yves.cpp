#include <iostream>
#include "Personne.h"
#include "EquipeMoto.h"


void AffichePersonne(Personne* Perso) {
    cout << "Personne(" << Perso << ") : " << Perso->GetNom() << endl;
}

Personne* CreerPersonne() {
    string nom;
    cout << "Saisissez le nom de la personne : ";
    cin >> nom;
    return new Personne(nom);
}

void AfficheEquipeMoto(EquipeMoto* equipe) {
    cout << "Equipe (" << equipe << ")   Nom equipe : " << equipe->GetNom() << " a pour manager : " << equipe->GetManager()->GetNom() << endl;
    Personne** pilotes = equipe->GetPilotes();
    for (int i = 0;i<3;i++) {
        if (pilotes[i] != nullptr) {
            cout << "       -> Pilote[" << i << "]= " << pilotes[i]->GetNom() << endl;
        }
        else { cout << "        ->Pas de pilote [" << i << "]\n"; }
    }
}

int main()
{
    Personne pilote_1("Fabio");
    Personne* pilote_2 = CreerPersonne();

    AffichePersonne(&pilote_1);
    AffichePersonne(pilote_2);

    EquipeMoto* equipe_1 = new EquipeMoto("YMF", "Jarvis");
    equipe_1->AddPilotes(0, &pilote_1);
    equipe_1->AddPilotes(1, pilote_2);

    AfficheEquipeMoto(equipe_1);


    delete pilote_2;
    delete equipe_1;

}