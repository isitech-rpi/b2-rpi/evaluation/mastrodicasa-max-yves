#pragma once
#include <string>
#include <iostream>
using namespace std;
class Personne
{
private :
	string nom;

public:
	Personne(string nom);
	~Personne(void);

	string GetNom() { return this->nom; }
	string GetNom() const { return this->nom; }
	
	void SetNom(string nom);
};

